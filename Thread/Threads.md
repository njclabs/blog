# Mule & Java Threads

This blog tries to clarify some of the concepts revolving around Threads in Java and Mule Runtime, how they are related. A good knowledge of different threads is necessary to to understand profiling and finetuning mule applications. This knowledge will be also useful when performing root cause analysis.

This blog looks in to following topics.

- Threads in Java
- Threads in Mule
- Different threads
- where it is used
- difference between concurrency vs multi-threading with respect to mule
- cpu thread vs jvm thread
- grizzly thread pool
- concurrent request behavior

## What is a Thread ?

A thread is a lightweight process that runs inside a process, which enables a program to run multiple tasks concurrently. This helps a single program to support multiple users, multiple tasks or multiple systems in a single application.

There are two types of threads
- software threads ( Provided by the Operating System)
- hardware threads ( Provided by CPU and Hardware )

## How Threads Work ?

Generally threads work by either distributing tasks to different CPU or Hardware ( Hardware Thread ) or using Time Slice ( Software Thread ). If a hardware thread is used, a dedicated hardware is assigned to perform the task, for instance in case of a CPU its going to be a core in a multi core processor.

In case of a software threading, available CPU cycles are distributed between threads and parts of different threads are run sequentially one after the other in the CPU.

Both the approaches gives an impression of multiple process running parallelly.

## What is a Thread in Java ?

A Java Thread is an abstraction of hardware and software threads depending on the JVM implementation. It is defined by using a Thread Class.
A java application can have multiple threads of execution running concurrently in a JVM.

Every thread has a priority.
- Threads with higher priority are executed in preference to threads with lower priority.
- Each thread may or may not also be marked as a daemon.
- When code running in some thread creates a new Thread object, the new thread has its priority initially set equal to the priority of the creating thread, and is a daemon thread if and only if the creating thread is a daemon.

When a Java Virtual Machine starts up, there is usually a single non-daemon thread (which typically calls the method named main of some designated class). The Java Virtual Machine continues to execute threads until either of the following occurs:

The exit method of class Runtime has been called and the security manager has permitted the exit operation to take place.
All threads that are not daemon threads have died, either by returning from the call to the run method or by throwing an exception that propagates beyond the run method.

JVM gets its thread from the underlying operating system.

## Understanding JVM Threads

Lets create an run the following java application and profile the same.

Create a file Hello.java with following line of code.

```java
public class Hello {
    public static void main(String[] args) {
      int x = 0;
      while ( x < 1 )
      {
        System.out.println("Hello World");
      }
    }
}
```

Open terminal and compile and run the code

```
javac Hello.java
java hello.java
```

You should have a Hello world printed over endlessly. You can use `ctrl+c` to stop the program when done testing.

### Get the PID of your Java process

The first piece of information you will need to be able to obtain a thread dump is your Java process's PID.
The Java JDK ships with the jps command which lists all Java process ids. You can run this command like this:

```
C:\Users\mule>jps
15104 Jps
13508 Hello
```

13508 is the process id of our java application. ( our JVM is running Hello application with this ID )

### Lets get a thread dump

* what is a thread dump *
A thread dump is a snapshot of live threads in a JVM process at a given point of time. A process in this case is our Hello Application.

Lets take a thread dump and see what all are inside there. Run the following command to obtain the result as shown below.

```
C:\Users\mule>jstack -l 13508
```

```
2020-04-15 00:18:52
Full thread dump OpenJDK 64-Bit Server VM (25.242-b08 mixed mode):

"Service Thread" #9 daemon prio=9 os_prio=0 tid=0x000001e767109800 nid=0x32e4 runnable [0x0000000000000000]
   java.lang.Thread.State: RUNNABLE

   Locked ownable synchronizers:
        - None

"C1 CompilerThread2" #8 daemon prio=9 os_prio=2 tid=0x000001e76708e800 nid=0x2f24 waiting on condition [0x0000000000000000]
   java.lang.Thread.State: RUNNABLE

   Locked ownable synchronizers:
        - None

"C2 CompilerThread1" #7 daemon prio=9 os_prio=2 tid=0x000001e76707f800 nid=0x1d80 waiting on condition [0x0000000000000000]
   java.lang.Thread.State: RUNNABLE

   Locked ownable synchronizers:
        - None

"C2 CompilerThread0" #6 daemon prio=9 os_prio=2 tid=0x000001e76707a000 nid=0x13c0 waiting on condition [0x0000000000000000]
   java.lang.Thread.State: RUNNABLE

   Locked ownable synchronizers:
        - None

"Attach Listener" #5 daemon prio=5 os_prio=2 tid=0x000001e767078000 nid=0x2ffc waiting on condition [0x0000000000000000]
   java.lang.Thread.State: RUNNABLE

   Locked ownable synchronizers:
        - None

"Signal Dispatcher" #4 daemon prio=9 os_prio=2 tid=0x000001e767076800 nid=0x22e8 runnable [0x0000000000000000]
   java.lang.Thread.State: RUNNABLE

   Locked ownable synchronizers:
        - None

"Finalizer" #3 daemon prio=8 os_prio=1 tid=0x000001e766919000 nid=0x1954 in Object.wait() [0x0000000cbcaff000]
   java.lang.Thread.State: WAITING (on object monitor)
        at java.lang.Object.wait(Native Method)
        - waiting on <0x0000000760308ee8> (a java.lang.ref.ReferenceQueue$Lock)
        at java.lang.ref.ReferenceQueue.remove(ReferenceQueue.java:144)
        - locked <0x0000000760308ee8> (a java.lang.ref.ReferenceQueue$Lock)
        at java.lang.ref.ReferenceQueue.remove(ReferenceQueue.java:165)
        at java.lang.ref.Finalizer$FinalizerThread.run(Finalizer.java:216)

   Locked ownable synchronizers:
        - None

"Reference Handler" #2 daemon prio=10 os_prio=2 tid=0x000001e767056000 nid=0x39dc in Object.wait() [0x0000000cbc9ff000]
   java.lang.Thread.State: WAITING (on object monitor)
        at java.lang.Object.wait(Native Method)
        - waiting on <0x0000000760306c08> (a java.lang.ref.Reference$Lock)
        at java.lang.Object.wait(Object.java:502)
        at java.lang.ref.Reference.tryHandlePending(Reference.java:191)
        - locked <0x0000000760306c08> (a java.lang.ref.Reference$Lock)
        at java.lang.ref.Reference$ReferenceHandler.run(Reference.java:153)

   Locked ownable synchronizers:
        - None

"main" #1 prio=5 os_prio=0 tid=0x000001e74abc5000 nid=0x25ac runnable [0x0000000cbc3ff000]
   java.lang.Thread.State: RUNNABLE
        at java.io.FileOutputStream.writeBytes(Native Method)
        at java.io.FileOutputStream.write(FileOutputStream.java:326)
        at java.io.BufferedOutputStream.flushBuffer(BufferedOutputStream.java:82)
        at java.io.BufferedOutputStream.flush(BufferedOutputStream.java:140)
        - locked <0x0000000760313170> (a java.io.BufferedOutputStream)
        at java.io.PrintStream.write(PrintStream.java:482)
        - locked <0x00000007603130c0> (a java.io.PrintStream)
        at sun.nio.cs.StreamEncoder.writeBytes(StreamEncoder.java:221)
        at sun.nio.cs.StreamEncoder.implFlushBuffer(StreamEncoder.java:291)
        at sun.nio.cs.StreamEncoder.flushBuffer(StreamEncoder.java:104)
        - locked <0x000000076031e850> (a java.io.OutputStreamWriter)
        at java.io.OutputStreamWriter.flushBuffer(OutputStreamWriter.java:185)
        at java.io.PrintStream.write(PrintStream.java:527)
        - eliminated <0x00000007603130c0> (a java.io.PrintStream)
        at java.io.PrintStream.print(PrintStream.java:669)
        at java.io.PrintStream.println(PrintStream.java:806)
        - locked <0x00000007603130c0> (a java.io.PrintStream)
        at Hello.main(Hello.java:7)

   Locked ownable synchronizers:
        - None

"VM Thread" os_prio=2 tid=0x000001e767032000 nid=0x324c runnable

"GC task thread#0 (ParallelGC)" os_prio=0 tid=0x000001e74abdc800 nid=0x5fc runnable

"GC task thread#1 (ParallelGC)" os_prio=0 tid=0x000001e74abdf000 nid=0x2110 runnable

"GC task thread#2 (ParallelGC)" os_prio=0 tid=0x000001e74abe2000 nid=0x37b4 runnable

"GC task thread#3 (ParallelGC)" os_prio=0 tid=0x000001e74abe3800 nid=0x32c0 runnable

"VM Periodic Task Thread" os_prio=2 tid=0x000001e76711c800 nid=0x2db4 waiting on condition

JNI global references: 4

```

The above lists some of the threads Java Virtual Machine has created for our Application to run.
Broadly they can be classified into two

- JVM Internal Threads
- Application Threads

Threads used by the JVM internally

|  Thread Type	 			| What they Do	   |
| ---			 			| --- |
| VM thread  	 			| This thread waits for operations to appear that require the JVM to reach a safe-point. The reason these operations have to happen on a separate thread is because they all require the JVM to be at a safe point where modifications to the heap can not occur. The type of operations performed by this thread are "stop-the-world" garbage collections, thread stack dumps, thread suspension and biased locking revocation. |
| Periodic task thread  	| This thread is responsible for timer events (i.e. interrupts) that are used to schedule execution of periodic operations  |
|  GC threads 				|  These threads support the different types of garbage collection activities that occur in the JVM  |
|  Compiler threads 		| These threads compile byte code to native code at runtime    |
|  Signal dispatcher thread | This thread receives signals sent to the JVM process and handle them inside the JVM by calling the appropriate JVM methods.   |
| Reference Handler | This thread runs the Classloader Reference class which defines the operations common to all reference objects , which is later used by Garbage Collector. All classes that are loaded contain a reference to the classloader that loaded them. In turn the classloader also contains a reference to all classes that it has loaded. |

Threads used by the Application running inside our JVM

|  Thread Type	 			| What they Do	   |
| ---			 			| --- |
| main |	Starting thread of our Hello application |

## Lets go MultiThreading

Create a HelloThread.java and put the following code in

``` java
class MyTask extends Thread {
  MyTask(String s)
  {
    setName(s);
  }
  public void run() {
    System.out.println("Running Thread... " + getName());
    try {
      sleep(100000);
    } catch(Exception e){

    }
  }
}

public class HelloThread {

    public static void main(String[] args) {
      MyTask t1 = new MyTask("Task 1");
      MyTask t2 = new MyTask("Task 2");
      t1.start();
      t2.start();
      int x = 0;
      while ( x < 1 )
      {
      }
    }
}

```

Compile the HelloThread.java code and run the application. Open a new termial and take a thread dump as shown below.

```
F:\blog>jps
8672 HelloThread
16312 Jps
```

```
F:\blog>jstack -l 8672
2020-04-15 02:06:59
Full thread dump OpenJDK 64-Bit Server VM (25.242-b08 mixed mode):

"Task 2" #11 prio=5 os_prio=0 tid=0x0000028d7e217000 nid=0x2454 waiting on condition [0x000000f3b3fff000]
   java.lang.Thread.State: TIMED_WAITING (sleeping)
        at java.lang.Thread.sleep(Native Method)
        at MyTask.run(HelloThread.java:9)

   Locked ownable synchronizers:
        - None

"Task 1" #10 prio=5 os_prio=0 tid=0x0000028d7e216000 nid=0x1d08 waiting on condition [0x000000f3b3eff000]
   java.lang.Thread.State: TIMED_WAITING (sleeping)
        at java.lang.Thread.sleep(Native Method)
        at MyTask.run(HelloThread.java:9)

   Locked ownable synchronizers:
        - None

"Service Thread" #9 daemon prio=9 os_prio=0 tid=0x0000028d7e1b6800 nid=0x1420 runnable [0x0000000000000000]
   java.lang.Thread.State: RUNNABLE

   Locked ownable synchronizers:
        - None

"C1 CompilerThread2" #8 daemon prio=9 os_prio=2 tid=0x0000028d7e13c000 nid=0x2700 waiting on condition [0x0000000000000000]
   java.lang.Thread.State: RUNNABLE

   Locked ownable synchronizers:
        - None

"C2 CompilerThread1" #7 daemon prio=9 os_prio=2 tid=0x0000028d7e12f000 nid=0x1690 waiting on condition [0x0000000000000000]
   java.lang.Thread.State: RUNNABLE

   Locked ownable synchronizers:
        - None

"C2 CompilerThread0" #6 daemon prio=9 os_prio=2 tid=0x0000028d7e12a000 nid=0x27e0 waiting on condition [0x0000000000000000]
   java.lang.Thread.State: RUNNABLE

   Locked ownable synchronizers:
        - None

"Attach Listener" #5 daemon prio=5 os_prio=2 tid=0x0000028d7e128800 nid=0x1a78 waiting on condition [0x0000000000000000]
   java.lang.Thread.State: RUNNABLE

   Locked ownable synchronizers:
        - None

"Signal Dispatcher" #4 daemon prio=9 os_prio=2 tid=0x0000028d7e127800 nid=0x1940 runnable [0x0000000000000000]
   java.lang.Thread.State: RUNNABLE

   Locked ownable synchronizers:
        - None

"Finalizer" #3 daemon prio=8 os_prio=1 tid=0x0000028d7d9cc000 nid=0x2900 in Object.wait() [0x000000f3b36ff000]
   java.lang.Thread.State: WAITING (on object monitor)
        at java.lang.Object.wait(Native Method)
        - waiting on <0x0000000760308ee8> (a java.lang.ref.ReferenceQueue$Lock)
        at java.lang.ref.ReferenceQueue.remove(ReferenceQueue.java:144)
        - locked <0x0000000760308ee8> (a java.lang.ref.ReferenceQueue$Lock)
        at java.lang.ref.ReferenceQueue.remove(ReferenceQueue.java:165)
        at java.lang.ref.Finalizer$FinalizerThread.run(Finalizer.java:216)

   Locked ownable synchronizers:
        - None

"Reference Handler" #2 daemon prio=10 os_prio=2 tid=0x0000028d7e105800 nid=0x3780 in Object.wait() [0x000000f3b35ff000]
   java.lang.Thread.State: WAITING (on object monitor)
        at java.lang.Object.wait(Native Method)
        - waiting on <0x0000000760306c08> (a java.lang.ref.Reference$Lock)
        at java.lang.Object.wait(Object.java:502)
        at java.lang.ref.Reference.tryHandlePending(Reference.java:191)
        - locked <0x0000000760306c08> (a java.lang.ref.Reference$Lock)
        at java.lang.ref.Reference$ReferenceHandler.run(Reference.java:153)

   Locked ownable synchronizers:
        - None

"main" #1 prio=5 os_prio=0 tid=0x0000028d6abd3800 nid=0x3bac runnable [0x000000f3b2fff000]
   java.lang.Thread.State: RUNNABLE
        at HelloThread.main(HelloThread.java:24)

   Locked ownable synchronizers:
        - None

"VM Thread" os_prio=2 tid=0x0000028d7e0e2800 nid=0x3e34 runnable

"GC task thread#0 (ParallelGC)" os_prio=0 tid=0x0000028d6abea800 nid=0x19d8 runnable

"GC task thread#1 (ParallelGC)" os_prio=0 tid=0x0000028d6abee000 nid=0x1684 runnable

"GC task thread#2 (ParallelGC)" os_prio=0 tid=0x0000028d6abef800 nid=0x2a68 runnable

"GC task thread#3 (ParallelGC)" os_prio=0 tid=0x0000028d6abf1000 nid=0x2c08 runnable

"VM Periodic Task Thread" os_prio=2 tid=0x0000028d7e1c1800 nid=0x10f4 waiting on condition

JNI global references: 4
```

>Notice the application running concurrent threads. Below you can see three different threads, a main thread that continue to execute the program, Task 1 and Task 2 that has been created and executed concurrently along with the main thread.


```
"Task 2" #11 prio=5 os_prio=0 tid=0x0000028d7e217000 nid=0x2454 waiting on condition [0x000000f3b3fff000]
   java.lang.Thread.State: TIMED_WAITING (sleeping)
        at java.lang.Thread.sleep(Native Method)
        at MyTask.run(HelloThread.java:9)

   Locked ownable synchronizers:
        - None

"Task 1" #10 prio=5 os_prio=0 tid=0x0000028d7e216000 nid=0x1d08 waiting on condition [0x000000f3b3eff000]
   java.lang.Thread.State: TIMED_WAITING (sleeping)
        at java.lang.Thread.sleep(Native Method)
        at MyTask.run(HelloThread.java:9)

   Locked ownable synchronizers:
        - None


"main" #1 prio=5 os_prio=0 tid=0x0000028d6abd3800 nid=0x3bac runnable [0x000000f3b2fff000]
   java.lang.Thread.State: RUNNABLE
        at HelloThread.main(HelloThread.java:24)


```
